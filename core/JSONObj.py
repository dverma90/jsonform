# use this file as a way to retrive and save changes to json file

import os
import sys
import datetime
import json


class JSONObj(object):
	def __init__(self, file_name):
		self.file_name = file_name

	def get_json_data(self, **kwargs):
		"""
		Retrive based on condition speified
		"""
		if not len(kwargs):
			raise Exception("Please specify atleast one criteria")
		for row in self.read():
			okey = False
			for key in kwargs:
				if kwargs[key]!=row[key]:
					okey = False
					break
				else:
					okey = True
			if okey:
				return row
		else:
			pass # or do what you want to do
		return {}

	def read(self,):
		fp = None
		try:
			fp = open(self.file_name)
			data = json.loads(fp.read())
			for row in data:
				yield row
		finally:
			if hasattr(fp, 'close'):
				fp.close()
	def insert_json(self, **kwargs):
		"""
		Create a new entry in the json file

		Not done in efficient way
		"""
		fp = open(self.file_name, 'r')
		data = json.loads(fp.read())
		fp.close()
		last_row = data[-1] or {}

		nid = int(last_row.get('id', -1) or random.random()*1000000)+1
		fp = open(self.file_name, 'w')
		kwargs['id'] = nid
		data.append(kwargs)
		fp.write(json.dumps(data))
		fp.close()



	def update_json(self, id=None, **kwargs):
		"""
		This is performance lazy
		What is does it reads all content from file_name
		and write whole file content once again

		Note: try doing this using python generator and any other beter technology

		And also this is not coded for nested json object, dictionary inside another dictionary
		"""
		fp = None
		data = []
		if not id:
			self.insert_json(**kwargs)
			return True
		try:
			fp = open(self.file_name)
			data = json.loads(fp.read())
			fp.close()
			for row in data:
				if row['id']==id:
					for key in kwargs:
						row[key] = kwargs[key]
		finally:
			if hasattr(fp, 'close'):
				fp.close()
		if len(data):
			self.insert_fixture(data)


	def insert_fixture(self, data=None):
		"""
		Beware this will replace the whole file content
		"""
		fp = None
		data = data or []
		try:
			fp = open(self.file_name, 'w')
			if not data:
				for i in xrange(1, 100+1):
					data.append({'id':i, 'name': '{index}-test{index}'.format(index=i)}) 
			row = json.dumps(data)
			fp.write(row)
			fp.close()
		finally:
			if hasattr(fp, 'close'):
				fp.close()








