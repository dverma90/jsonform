# Create your views here.

# create a view to show a django from
# load that form with json data from json file
# and then save that from data to that json file


from django.shortcuts import render
from django.views.generic.base import TemplateView, View
from core.JSONObj import JSONObj


JSON_FILE_NAME = 'W:/CODE_PRACTICE/jsonform/core/myjson.json'

from .forms import JsonForm

class JSONView(View):
	def get(self, request):
		idn =  int(request.GET.get('id', 0) or 0)
		if idn:
			jfp = JSONObj(JSON_FILE_NAME)
			record = jfp.get_json_data(id=idn)
			form = JsonForm(record)
		else:
			form = JsonForm()
		return render(request, 'myapp/myjson.html', {'form':form})
	def post(self, request):
		form = JsonForm(request.POST)
		if form.is_valid():
			jfp = JSONObj(JSON_FILE_NAME)
			print request.POST
			idn = int(request.POST.get('id', 0) or 0)
			name = request.POST.get('name', 'undefined')
			jfp.update_json(idn, name=name)
			return render(request, "myapp/confirm.html", {})
		else:
			return render(request, "myapp/myjson.html", {'form':form})

